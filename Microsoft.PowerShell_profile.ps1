#************************** General Variables for the Profile **************************#
# Sets the directory you open up into when you start Powershell
#Set-Location $env:userprofile\Documents\WindowsPowerShell\


# Console Settings
#Sets the title of the Window
$host.ui.RawUI.WindowTitle = "($env:userdomain\$env:username) Windows PowerShell"

#***************************Import Modules******************************#
import-module dbatools

#************************** Profile Functions **************************#
function prompt {
  $p = Split-Path -leaf -path (Get-Location)
  "$p> "
}

#Change to DBA_Scripts Directory
function Go-DBA_Scripts {
    Set-Location <somedir>
}

#Get UTC time
Function Get-UTC {
  $TimeNow = Get-Date
  get-date $TimeNow -f "MMddyy HH:mm:ss"
  $TimeNow.ToUniversalTime().ToString("MMddyy HH:mm:ss")
}

#Get Logical drive information of a given computer`
Function Get-DriveInfo {
  Param ([string] $c)
  Get-WmiObject -Class Win32_logicaldisk -Computername $c
}


function get-childitemsorted {
  param ()
  Get-ChildItem | sort lastwritetime
}

#**************************Aliases **************************#
Set-Alias im Import-Module
Set-Alias fu Find-ADUser
Set-Alias utc Get-UTC
Set-Alias DI Get-DriveInfo
Set-Alias gh Get-Help
set-alias ll get-childitemsorted

